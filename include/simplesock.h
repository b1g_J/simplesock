#ifndef SIMPLESOCK_H
#define SIMPLESOCK_H

#include <iostream>
#include <string>
#include <boost/asio.hpp>

using namespace boost::asio;
using ip::tcp;

class SimpleSock {
public:
    SimpleSock(const std::string& address, int port);
    SimpleSock(const SimpleSock& sock);
    ~SimpleSock();

    int send(const std::string& message);
    int sendline(const std::string& message);

    std::string recvline(int lines=1);
    std::string recvuntil(char c);
    
private:
    boost::asio::io_service io_service;
    tcp::socket* socket;
    std::string address;
    int port;
};

#endif
